<?php

namespace App\FFMpeg\Video;

class X264 extends \FFMpeg\Format\Video\X264
{
    /**
     * {@inheritDoc}
     */
    public function getAvailableVideoCodecs()
    {
        return array('libx264', 'h264');
    }
}