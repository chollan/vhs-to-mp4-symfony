<?php

namespace App\FFMpeg\Audio;

use FFMpeg\Format\Audio\DefaultAudio;

class Aac extends DefaultAudio
{
    public function __construct()
    {
        $this->audioCodec = 'aac';
    }

    /**
     * {@inheritDoc}
     */
    public function getAvailableAudioCodecs()
    {
        return array('aac');
    }
}
