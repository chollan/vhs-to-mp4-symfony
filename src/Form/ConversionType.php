<?php

namespace App\Form;

use App\Entity\Conversion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ConversionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if(!$options['include_filename']){
            $builder->add('initialFileName', TextType::class, [
                'disabled' => true,
                'help' => 'You are not allowed to edit the file name of an already saved job.'
            ]);
        }else{
            $builder->add('initialFileName', TextType::class, [
                'data' => date('m-d-Y-H-i-s').'.mp4',
                'help' => 'This can not be changed once saved.'
            ]);
        }
        $builder
            /*->add('initialFileName', TextType::class, [
                'data' => date('m-d-Y-H-i-s').'.mp4'
            ])*/
            ->add('notes')
            ->add('image', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        //'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg', 'image/jpg', 'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid jpeg or png image',
                    ])
                ]
            ])
            ->add('save', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Conversion::class,
            'include_filename' => true
        ]);
        $resolver->setAllowedTypes('include_filename', 'boolean');
    }
}
