<?php

namespace App\Entity;

use App\Repository\ConversionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConversionRepository::class)
 */
class Conversion
{
    const STATUS_IN_PROGRESS = 'In Progress';
    const STATUS_COMPLETE = 'Complete';
    const STATUS_NOT_STARTED = 'Not Started';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $initialFileName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $started;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $completed;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInitialFileName(): ?string
    {
        return $this->initialFileName;
    }

    public function setInitialFileName(string $initialFileName): self
    {
        $this->initialFileName = $initialFileName;

        return $this;
    }

    public function getStarted(): ?\DateTimeInterface
    {
        return $this->started;
    }

    public function setStarted(\DateTimeInterface $started): self
    {
        $this->started = $started;

        return $this;
    }

    public function getCompleted(): ?\DateTimeInterface
    {
        return $this->completed;
    }

    public function setCompleted(?\DateTimeInterface $completed): self
    {
        $this->completed = $completed;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getStatus(): string
    {
        if(is_null($this->started)){
            return self::STATUS_NOT_STARTED;
        }else if(!is_null($this->started) && is_null($this->completed)){
            return self::STATUS_IN_PROGRESS;
        }else if(!is_null($this->started) && !is_null($this->completed)){
            return self::STATUS_COMPLETE;
        }
    }

    public function isInProgress(): bool
    {
        return $this->getStatus() === self::STATUS_IN_PROGRESS;
    }

    public function isComplete(): bool
    {
        return $this->getStatus() === self::STATUS_COMPLETE;
    }

    public function isNotStarted(): bool
    {
        return $this->getStatus() === self::STATUS_NOT_STARTED;
    }

    public function isStarted(): bool
    {
        return $this->getStatus() == self::STATUS_IN_PROGRESS;
    }

    public function getPid(): ?int
    {
        return $this->pid;
    }

    public function setPid(?int $pid): self
    {
        $this->pid = $pid;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
