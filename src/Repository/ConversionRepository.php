<?php

namespace App\Repository;

use App\Entity\Conversion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Conversion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Conversion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Conversion[]    findAll()
 * @method Conversion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConversionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Conversion::class);
    }

    public function findRunning(){
        $query = $this->createQueryBuilder('c')
            ->andWhere('c.started is not null')
            ->andwhere('c.completed is null')
            //->setParameters(['emptyString' => '', 'null' => null])
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            //->getOneOrNullResult()
        ;
        return $query->getOneOrNullResult();
        //dump($query->getSQL());exit;
    }

    public function getMaxId(){
        $query = $this->createQueryBuilder('c')
            ->select('max(c.id)')
            ->setMaxResults(1)
            ->getQuery()
            //->getOneOrNullResult()
        ;
        $result = $query->getOneOrNullResult();
        if(is_null($result)){
            return 0;
        }
        return $result[1];
    }

    // /**
    //  * @return Conversion[] Returns an array of Conversion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Conversion
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
