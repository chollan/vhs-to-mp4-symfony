<?php

namespace App\Command;

use App\Service\FFMpegService;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use App\FFMpeg\Audio\Aac;
use App\FFMpeg\Video\X264;
use FFMpeg\Filters\AdvancedMedia\CustomComplexFilter;
use FFMpeg\Filters\Video\ClipFilter;
use FFMpeg\Media\Clip;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class Transcode extends Command
{
    protected static $defaultName = 'transcode:run';
    private $input;
    private $output;
    private $ffmpegService;
    private $logger;

    function __construct(string $name = null, FFMpegService $FFMpegService, LoggerInterface $logger)
    {
        $this->ffmpegService = $FFMpegService;
        $this->logger = $logger;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Transcode from VHS to MP4')
            ->addArgument('minutes', InputArgument::REQUIRED, 'how many minutes should this transcode job run for')
            ->addArgument('filename', InputArgument::OPTIONAL, 'what should this file name be called?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //sleep(10);
        //return true;

        set_time_limit(0);
        $this->input = $input;
        $this->output = $output;
        $this->logger->debug('execute called');

        $minutes = $input->getArgument('minutes');
        $filename = $input->getArgument('filename');

        if(is_null($filename)){
            $filename = date('m-d-Y-H-i-s').'.mp4';
        }

        //$this->ffmpegService->startEncoding($filename, $minutes);
        $this->runFfmpeg($filename, $minutes);

        return Command::SUCCESS;
    }

    private function runFfmpeg(string $filename, int $minutes){

        // start ffmpeg
        $ffmpeg = FFMpeg::create([ 'timeout' => '0' ]);
        $advancedMedia = $ffmpeg->openAdvanced(array('hw:0', '/dev/video0'));
        /*$advancedMedia->filters()
            ->custom('[0:v][1:v]', 'hstack', '[v]');*/
        //$X264 = new X264('aac', 'h264');
        $X264 = new X264('aac', 'libx264');
        //$X264->setInitialParameters(['-thread_queue_size', '512']);
        $X264->setAdditionalParameters([
            '-pix_fmt', 'yuv420p',
            '-t', (string)TimeCode::fromSeconds($minutes*60),
            '-crf', '23'
            //'-t', '00:00:10.00'
        ]);


        $advancedMedia
            ->setInitialParameters(['-f', 'alsa', '-thread_queue_size', '512'])
            ->map(array('0:a', '1:v'), $X264, $filename)
        ;
        $advancedMedia
            ->setAdditionalParameters(['-map', '0:a', '-map', '1:v', '-qmin', '10', '-qmax', '51', 'http://localhost:8090/feed1.ffm'])
        ;
        //dump($advancedMedia);exit;

        //dump($advancedMedia);
        //dump('ffmpeg '.$advancedMedia->getFinalCommand());
        //exit;
        $this->logger->info('calling');
        $advancedMedia->save();
        /*$ffmpeg->open('hw:0')
        $video = $ffmpeg
            ->open('/dev/video0')
        ;
        $format = new X264();
        $format
            ->setVideoCodec('v4l2')
            ->setAudioCodec()
        ;
        dump($ffmpeg);*/


        //TODO: Get vodeo status and stop when the video stopps
        // command to review the current frames:
        // ffprobe -v quiet -f lavfi -i "movie='http\:\/\/localhost\:8090\/live.flv',signalstats" -show_entries frame_tags=lavfi.signalstats.HUEAVG -of flat -read_intervals "%+#50" -print_format json   > test.json
        // (will output json file.  take off redirest at end)

        //TODO: RE-Transcode for a smaller file?  let's try doing the below command in the main script:
        //ffmpeg -i 11-30-2020-13-06-02.mp4 -vcodec libx264 -crf 30 output.mp4

    }

    private function getMetaDescription(){
        $helper = $this->getHelper('question');
        $question = new Question('Enter a description: ', '');
        return $helper->ask($this->input, $this->output, $question);
    }

    private function getFileName(){
        $helper = $this->getHelper('question');
        $default = date('m-d-Y-H-i-s').'.mp4';
        $question = new Question('What is te name of the file? <info>['.$default.']</info> ', $default);
        $filename = $helper->ask($this->input, $this->output, $question);
        preg_match('/[a-z0-9\-\s]+\.mp4/i',$filename,$matches);
        if(count($matches) > 0){
            return $matches[0];
        }
        return false;
    }

    private function getMinutes(){
        $helper = $this->getHelper('question');
        $default = '120';
        $question = new Question('How many minutes? <info>['.$default.']</info> ', $default);
        $filename = $helper->ask($this->input, $this->output, $question);
        preg_match('/[\d]+/',$filename,$matches);
        if(count($matches) > 0){
            return $matches[0];
        }
        return false;
    }

}