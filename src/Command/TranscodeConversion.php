<?php

namespace App\Command;

use App\Entity\Conversion;
use App\Repository\ConversionRepository;
use App\Service\FFMpegService;
use Aws\Sns\SnsClient;
use Doctrine\ORM\EntityManagerInterface;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use App\FFMpeg\Video\X264;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class TranscodeConversion extends Command
{
    protected static $defaultName = 'transcode:run:conversion';
    private $input;
    private $output;
    private $ffmpegService;
    private $conversionRepository;
    private $conversion;
    private $em;
    private $kernel;
    private $parameterBag;

    function __construct(
        FFMpegService $FFMpegService,
        ConversionRepository $conversionRepository,
        EntityManagerInterface $em,
        KernelInterface $kernel,
        ParameterBagInterface $parameterBag,
        string $name = null
    ){
        $this->ffmpegService = $FFMpegService;
        $this->conversionRepository = $conversionRepository;
        $this->em = $em;
        $this->kernel = $kernel;
        $this->parameterBag = $parameterBag;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Transcode from VHS to MP4')
            ->addArgument('conversion_id', InputArgument::REQUIRED, 'ID of the converson job that we need to run')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // set the infinate timeout
        set_time_limit(0);

        // process some input
        $this->input = $input;
        $this->output = $output;

        $conversion_id = $input->getArgument('conversion_id');
        $this->conversion = $this->conversionRepository->find($conversion_id);
        if(!($this->conversion instanceof Conversion)){
            throw new InvalidParameterException('Conversion by the ID of '.$conversion_id.' not found');
            return 1;
        }

        $projectRoot = $this->kernel->getProjectDir();

        //$filename = $projectRoot.'/src/ConversionOutput/'.$this->conversion->getInitialFileName();
        $filename = '/nas/homeMovies/ToOrganize/'.$this->conversion->getInitialFileName();
        $this->runFfmpeg($filename, 480);

        return 0;
    }

    private function runFfmpeg(string $filename, int $minutes){

        // start ffmpeg
        $ffmpeg = FFMpeg::create([ 'timeout' => '0' ]);

        // open multiple inputs
        $advancedMedia = $ffmpeg->openAdvanced(array('hw:0', '/dev/video0'));

        // create a format
        $format = new X264('aac', 'libx264');
        $format->setAdditionalParameters([
            '-pix_fmt', 'yuv420p',
            '-t', (string)TimeCode::fromSeconds($minutes*60),
            '-crf', '23' // experimental feature
            //'-t', '00:00:10.00'
        ]);


        // set some more parameters for the ffmpeg job
        $advancedMedia
            ->setInitialParameters(['-f', 'alsa', '-thread_queue_size', '512'])
            ->map(array('0:a', '1:v'), $format, $filename)
            ->setAdditionalParameters(['-map', '0:a', '-map', '1:v', '-qmin', '10', '-qmax', '51', 'http://localhost:8090/feed1.ffm'])
        ;

        // we don't want to start the job like this, cause then we can't get the PID
        //$advancedMedia->save();

        // start the job like this in the background and get the returning PID
        $cmd = '/usr/bin/ffmpeg '.$advancedMedia->getFinalCommand() . ' > /tmp/ffmpeg.out 2>&1 & printf "%u" $!';
        $pid = exec($cmd);

        // save the start and the PID to the DB
        $this->conversion->setStarted(new \DateTime());
        $this->conversion->setPid($pid);

        // store everything
        $this->em->persist($this->conversion);
        $this->em->flush();

        // notify curtis
        $snsClient = new SnsClient([
            'credentials' => [
                'key'    => $this->parameterBag->get('aws_key'),
                'secret' => $this->parameterBag->get('aws_secret'),
            ],
            'region' => 'us-east-1',
            'version' => 'latest'
        ]);
        $snsClient->publish([
            'Message' => 'Start command executed.  unconfirmed ffmpeg on pid '.$pid,
            'PhoneNumber' => '+12165098523'
        ]);

        // finished!
        return 0;

        //TODO: Get vodeo status and stop when the video stopps
        // command to review the current frames:
        // ffprobe -v quiet -f lavfi -i "movie='http\:\/\/localhost\:8090\/live.flv',signalstats" -show_entries frame_tags=lavfi.signalstats.HUEAVG -of flat -read_intervals "%+#50" -print_format json   > test.json
        // (will output json file.  take off redirest at end)

        //TODO: RE-Transcode for a smaller file?  let's try doing the below command in the main script:
        //ffmpeg -i 11-30-2020-13-06-02.mp4 -vcodec libx264 -crf 30 output.mp4

    }
}