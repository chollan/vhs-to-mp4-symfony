<?php

namespace App\Command;

use App\Entity\Conversion;
use App\Repository\ConversionRepository;
use App\Service\FFMpegService;
use Aws\Sns\SnsClient;
use Doctrine\ORM\EntityManagerInterface;
use FFMpeg\FFProbe;
use FFMpeg\Filters\AdvancedMedia\CustomComplexFilter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Process\Exception\ProcessTimedOutException;
use Symfony\Component\Process\Process;

class TestForBlueScreen extends Command
{
    protected static $defaultName = 'transcode:checkForBlue';
    private $input;
    private $output;
    private $ffmpegService;
    private $conversionRepository;
    private $em;
    private $parameterBag;

    function __construct(
        ParameterBagInterface $parameterBag,
        FFMpegService $FFMpegService,
        ConversionRepository $conversionRepository,
        EntityManagerInterface $em,
        string $name = null
    ){

        $this->parameterBag = $parameterBag;
        $this->conversionRepository = $conversionRepository;
        $this->ffmpegService = $FFMpegService;
        $this->em = $em;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Check to see if there is a blue screen currently being streamed.')
            ->addOption('kill', null, InputOption::VALUE_NONE, 'kill the ffmpeg process if there is a process running')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $conversion = $this->conversionRepository->findRunning();

        if(!($conversion instanceof Conversion)){
            return 0;
        }

        $this->input = $input;
        $this->output = $output;

        // do we want to kill?
        $kill = $input->getOption('kill');

        // build the test command
        $cmd = 'ffprobe  -f lavfi -i "movie=\'http\:\/\/localhost\:8090\/live.flv\',signalstats" -show_entries frame_tags=lavfi.signalstats.HUEAVG -of flat -read_intervals "%+#100" -print_format json';

        // prepare the command
        $process = Process::fromShellCommandline($cmd);
        $process->setTimeout(30);

        // run the command
        try{
            $process->run();
        }catch (ProcessTimedOutException $e){
            return 0;
        }

        // get the resposne
        $response = $process->getOutput();
        //$response = $this->getTestData();

        // parse the response
        $aryResponse = json_decode($response, true);

        // do some math to confirm that we're on a blue screen
        $sum = 0;
        foreach($aryResponse['frames'] as $frame){
            $sum += $frame['tags']['lavfi.signalstats.HUEAVG'];
        }

        // calculate the averate
        $average = $sum/count($aryResponse['frames']);

        // if we're between 267 and 269, we're done recording
        if($average >= 267.0 && $average <= 269.0){
            if($kill){
                $result = exec('kill '.$conversion->getPid());
                if($this->output->isDebug())
                    $this->output->writeln('Attempted to kill process ID '.$conversion->getPid().' with result of: '.$result);
                $this->notifyCurtis('Attempted to kill process ID '.$conversion->getPid().' with result of: '.$result);
                $conversion->setCompleted(new \DateTime());
                $this->em->persist($conversion);
                $this->em->flush();
            }else{
                if($this->output->isDebug())
                    $this->output->writeln("Kill is not enabled, yet there is a process to kill: ".$conversion->getPid());
            }
        }else{
            if($this->output->isDebug()){
                $this->output->writeln("Conversion ID number ".$conversion->getId()." is still running.");
                $this->output->writeln('hue variance is about '.$average.', expecting between 267.0 and 269.0');
            }

        }


        //TODO: Get vodeo status and stop when the video stopps
        // command to review the current frames:
        // ffprobe -v quiet -f lavfi -i "movie='http\:\/\/localhost\:8090\/live.flv',signalstats" -show_entries frame_tags=lavfi.signalstats.HUEAVG -of flat -read_intervals "%+#50" -print_format json   > test.json
        // (will output json file.  take off redirest at end)



        //$this->ffmpegService->startEncoding($filename, $minutes);
        //$this->runFfmpeg($filename, $minutes);

        return 0;
    }

    private function notifyCurtis($message){
        // notify curtis
        $snsClient = new SnsClient([
            'credentials' => [
                'key'    => $this->parameterBag->get('aws_key'),
                'secret' => $this->parameterBag->get('aws_secret'),
            ],
            'region' => 'us-east-1',
            'version' => 'latest'
        ]);

        $result = $snsClient->publish([
            'Message' => $message,
            'PhoneNumber' => '+12165098523'
        ]);
    }

}
