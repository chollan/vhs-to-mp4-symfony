<?php

namespace App\Controller;

use App\Entity\Conversion;
use App\Form\ConversionType;
use App\Repository\ConversionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(): Response
    {
        /*$this->addFlash('success', 'This is an success message');
        $this->addFlash('info', 'This is an info message');
        $this->addFlash('error', 'This is an error message');
        $this->addFlash('warning', 'This is an warning message');*/
        $repo = $this->getDoctrine()->getRepository(Conversion::class);
        //dump($repo->findRunning());exit;
        return $this->render('default/index.html.twig', [
            'videos' => array_reverse($repo->findAll()),
        ]);
    }

    /**
     * @Route("/jobDetails/{conversion}", name="viewJob")
     */
    public function viewJob(Conversion $conversion): Response
    {
        $repo = $this->getDoctrine()->getRepository(Conversion::class);
        $previous = false;
        if($conversion->getId() > 1){
            $previous = $conversion->getId()-1;
        }
        $maxId = $repo->getMaxId();
        $next = false;
        if($conversion->getId() < $maxId){
            $next = $conversion->getId()+1;
        }


        return $this->render('default/viewJob.html.twig', [
            'conversion' => $conversion,
            'previous' => $previous,
            'next' => $next
        ]);
    }

    /**
     * @Route("/newJob", name="newJob")
     * @Route("/editJob/{conversion}", name="editJob")
     */
    public function saveConversion(Request $request,  Conversion $conversion = null): Response
    {
        if(is_null($conversion)){
            $conversion = new Conversion();
            $aryOptions['include_filename'] = true;
        }else{
            $aryOptions['include_filename'] = false;
        }

        $form = $this->createForm(ConversionType::class, $conversion, $aryOptions);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $conversionForm = $form->getData();
            $file = $form->get('image')->getData();;
            if($file){
                //$originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                //$safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                //$newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();
                $newFilename = md5(microtime()).'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $file->move(
                        'uploadedImages',
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                    $this->addFlash('error', 'Error saving file: '.$e->getMessage());
                }

                // convert the image for uploading
                $process = new Process([
                    'convert', $newFilename, '-resize', '800', $newFilename
                ]);

                // try to run the command and catch any errors
                try{
                    $process->run();
                }catch (\Exception $e){
                    $this->addFlash('error', 'Error converting file to a smaller format: '.$e->getMessage());
                }


                //delete the file if an old one exists before we update it.
                if(!is_null($conversionForm->getImage())){
                    $filename = 'uploadedImages/'.$conversionForm->getImage();
                    $filesystem = new Filesystem();
                    $filesystem->remove($filename);
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $conversionForm->setImage($newFilename);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($conversionForm);
            $em->flush();
            //return $this->redirectToRoute('default');
            return $this->redirectToRoute('viewJob', ['conversion' => $conversionForm->getId()]);
        }

        return $this->render('default/newJob.html.twig', [
            'form' => $form->createView(),
            'conversion' => $conversion
        ]);
    }

    /**
     * @Route("/startJob/{conversion}", name="startJob")
     */
    public function startConversion(Conversion $conversion, KernelInterface $kernel, ConversionRepository $conversionRepository): Response
    {
        // get the project dir
        $projectRoot = $kernel->getProjectDir();

        if($conversion->isComplete()){
            // don't restart a job.
            $this->addFlash('error', 'The job ID '.$conversion->getId().' is already complete.');
            return $this->redirectToRoute('default');
        }

        if($conversionRepository->findRunning() instanceof Conversion){
            // don't restart a job.
            $this->addFlash('error', 'There is already a job running.');
            return $this->redirectToRoute('default');
        }

        // build the main command
        // we wrapped this script so we can allow apache to start this without a password using sudo
        $cmd = 'sudo '.$projectRoot.'/src/Script/startWrapper.sh '.$conversion->getId().'';

        // run and get the PID
        shell_exec($cmd);

        // sleep so the db has time to update
        sleep(2);
        $this->addFlash('success', 'Command confirmed.');

        // return to our home screen
        return $this->redirectToRoute('default');
    }
}
