<?php

namespace App\Service;

use App\Entity\Conversion;
use App\FFMpeg\Video\X264;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use Symfony\Component\Process\Process;

class FFMpegService
{
    /*public function isConversionRunning(Conversion $conversion):bool {
        $process = Process::fromShellCommandline('ps waux |grep ffmpeg');
        $process->run();
        $response = $process->getOutput();
        dump(array_filter(explode(PHP_EOL, $response)));exit;
    }*/

    public function startEncoding(string $filename, int $minutes){
        $ffmpeg = FFMpeg::create([
            'timeout' => '0'
        ]);

        // open the devices
        $advancedMedia = $ffmpeg->openAdvanced(array('hw:0', '/dev/video0'));

        // set the format
        $format = new X264('aac', 'libx264');
        $format->setAdditionalParameters([
            '-pix_fmt', 'yuv420p',
            '-t', (string)TimeCode::fromSeconds($minutes*60),
            //'-vcodec', 'libx264'
            '-crf', '23' // compression
        ]);


        // set some additional parameters and mapping
        $advancedMedia
            ->setInitialParameters(['-f', 'alsa', '-thread_queue_size', '512'])
            ->map(array('0:a', '1:v'), $format, $filename)
        ;

        // add some streaming so we can watch it live.
        // this require ffserver to be running on the .170 server
        $advancedMedia
            ->setAdditionalParameters(['-map', '0:a', '-map', '1:v', '-qmin', '10', '-qmax', '51', 'http://localhost:8090/feed1.ffm'])
        ;

        // run the command
        $advancedMedia->save();

        // get the command
        //$aryCommand = array_merge(['/usr/bin/ffmpeg'], $advancedMedia->getFinalCommand());

        //$process = new Process();
        //$advancedMedia->save();
        /*$ffmpeg->open('hw:0')
        $video = $ffmpeg
            ->open('/dev/video0')
        ;
        $format = new X264();
        $format
            ->setVideoCodec('v4l2')
            ->setAudioCodec()
        ;
        dump($ffmpeg);*/


        //TODO: Get vodeo status and stop when the video stopps
        // command to review the current frames:
        // ffprobe -v quiet -f lavfi -i "movie='http\:\/\/localhost\:8090\/live.flv',signalstats" -show_entries frame_tags=lavfi.signalstats.HUEAVG -of flat -read_intervals "%+#50" -print_format json   > test.json
        // (will output json file.  take off redirest at end)
    }
}